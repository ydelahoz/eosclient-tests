#!/bin/bash
## EOS-1811: RFE: support for "hard links" in FUSE
mkdir -p /eos/user/e/eostest/tests/hardlink || exit 1
linktmp=$(mktemp -d /eos/user/e/eostest/tests/hardlink/${HOSTNAME}__XXXX)

touch "${linktmp}/source" || exit 2

/bin/ln "${linktmp}/source"  "${linktmp}/target" || exit 3

rm -rf "${linktmp}" || { echo "Error during cleanup of ${linktmp}" >&2; exit 4; }
echo "hardlink: all OK" >&2
exit 0
