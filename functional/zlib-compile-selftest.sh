#!/bin/bash
## download and recompile zlib (this runs the internal self-tests at the end)
## supposed to reproduce a small project compilation on EOS

mkdir -p /eos/user/e/eostest/tests/zlib || exit 1
rpmtmp=$(mktemp -d /eos/user/e/eostest/tests/zlib/${HOSTNAME}_zlib_XXXX)
rpmbuild --rebuild --define "_topdir ${rpmtmp}" http://linuxsoft.cern.ch/cern/centos/7/os/Source/SPackages/zlib-1.2.7-17.el7.src.rpm || exit 2
rm -rf "${rpmtmp}" || { echo "Error during cleanup of ${rpmtmp}" >&2; exit 3; }
echo "zlib: all OK" >&2
exit 0
