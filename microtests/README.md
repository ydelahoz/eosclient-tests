File system microtests
======================

We collect tests in the _tests_ directory and test inputs in the _inputs_ directory. When the test runs it creates a
temporary folder in the same location that the script is run.

The tests can be run in three ways:

    run.py: this runs the test and sends outputs to our graphite monitoring system
    run-testing.py: this is for interactively running the tests just to see if they work.
    run-ci.py: this is intended to be run by a CI system. Work in progress.

To add a new test, put it in the tests directory, assign it the next available number, and follow the API

   ./011_testname <tmp dir>
