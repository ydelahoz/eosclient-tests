#!/usr/bin/env python

import os
import sys
import commands
import time
import tempfile
import socket
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--eosdir', action='store', dest='eosdir', help='eos fuse mount directory to run the tests on')
args = parser.parse_args()

BINDIR=os.path.dirname(__file__)
TESTDIR=os.path.abspath(os.path.join(BINDIR, 'tests'))
WORKDIR=os.path.abspath(tempfile.mkdtemp(dir=args.eosdir))
INSTANCE=WORKDIR.split('/')[2]

if not os.path.isdir(TESTDIR):
  raise Exception('TESTDIR: %s does not exist' % TESTDIR)

if not os.path.isdir(WORKDIR):
  raise Exception('WORKDIR: %s does not exist' % WORKDIR)

result = 0

for test in sorted(os.listdir(TESTDIR)):
  bin = os.path.join(TESTDIR,test)
  cmd = 'timeout 300 %s %s' % (bin, WORKDIR)

  start = time.time()
  status, output = commands.getstatusoutput(cmd)
  # Test failed if status is non-zero.
  result = 1 if status > 0 else result
  end = time.time()
  duration = end - start
  testname = test.split('_', 1)[1]
  message = 'eos.clients.fuse.%s.microtests.%s_ms time:%.3f\n status: %d\n command: %s\n output: %s\n' % (INSTANCE, testname, duration * 1000, status, cmd, output)
  print message

sys.exit(result)
