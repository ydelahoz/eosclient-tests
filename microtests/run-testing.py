#!/usr/bin/env python

import os
import commands
import time
import tempfile
import shutil
import socket

BINDIR=os.path.dirname(__file__)
TESTDIR=os.path.abspath(os.path.join(BINDIR, 'tests'))
WORKDIR=os.path.abspath(tempfile.mkdtemp(dir=BINDIR))
INSTANCE=WORKDIR.split('/')[2]

CARBON_HOST = 'filer-carbon.cern.ch'
CARBON_PORT = 2003

def send_message(message, timestamp = None):
  if timestamp is None:
    timestamp = time.time()

  print '%s %d' % (message, timestamp)
  return
  conn = socket.create_connection((CARBON_HOST, CARBON_PORT))
  conn.send('%s %d\n' % (message, timestamp))
  conn.close()

if not os.path.isdir(TESTDIR):
  raise Exception('TESTDIR: %s does not exist' % TESTDIR)

if not os.path.isdir(WORKDIR):
  raise Exception('TESTDIR: %s does not exist' % TESTDIR)

for test in sorted(os.listdir(TESTDIR)):
  bin = os.path.join(TESTDIR,test)
  cmd = 'timeout 300 %s %s' % (bin, WORKDIR)

  start = time.time()
  status, output = commands.getstatusoutput(cmd)
  end = time.time()
  duration = end - start
  testname = test.split('_', 1)[1]
  message = 'eos.clients.fuse.%s.microtests.%s_ms %.3f' % (INSTANCE, testname, duration * 1000)
  send_message(message, int(start))

# don't rm the test dir, done in a daily cron.
#shutil.rmtree(WORKDIR)
