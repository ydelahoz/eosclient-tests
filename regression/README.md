This area is mean to hold regression tests, i.e.
tests for things that were once broken, and are believed
to be fixed in later releases.

Naming convention:
* please use the short name of the relevant JIRA ticket (EOS-123) as
  first part of the filename



