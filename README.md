Collection point for various EOSFUSE related test cases.
========================================================

These should be run as the _eostest_ user into an area writeable by that user (e.g /eos/user/e/eostest/tests/).
A correspoding keytab in installed on machines in the _eostest_ cluster, in /etc/krb5.keytab.eostest. To use, please run

    kdestroy; kinit -kt /etc/krb5.keytab.eostest eostest; eosfusebind